package br.com.muxi.ebo.cards;


public class Luhn {

    private static final char[][] digitForLuhnComplement = {
            //0    1    2    3    4    5    6    7    8    9
            {'0', '9', '8', '7', '6', '5', '4', '3', '2', '1'},  // parity: 0
            {'0', '9', '4', '8', '3', '7', '2', '6', '1', '5'}   // parity: 1
    };
    private static final int[][]  luhnWeight             = {
            {0, 1, 2, 3, 4, 5, 6, 7, 8, 9},  // parity: 0
            {0, 2, 4, 6, 8, 1, 3, 5, 7, 9}   // parity: 1
    };

    /**
     * Check if a numeric string is valid according to the Luhn Algorithm, i.e., it has a valid check digit.
     * @param number      a numeric string.
     * @return true if {@code number} is a valid luhn numeric string
     * @see <a href="https://en.wikipedia.org/wiki/Luhn_algorithm">Luhn Algorithm - Wikipedia</a>
     */
    public static boolean isValid(String number) {
        return (luhnSum(number.toCharArray()) % 10) == 0;
    }

    /**
     * Replace the numeric substring {@code number[begin:]} by {@code replacement} plus a calculated digit such that
     * the result is a valid Luhn number.
     *
     * @param number      the original numeric string.
     * @param begin       the starting position to replace.
     * @param replacement the replacement substring that must fit on target position {@code begin} in {@code number}.
     * @return a valid luhn numeric string.
     */
    public static String replace(String number, int begin, String replacement) {
        final char[] digits = number.toCharArray();

        final int lastDigitOnReplacement = begin + replacement.length();
        final int end = lastDigitOnReplacement + 1;
        if (replacement.length() > 0) {
            System.arraycopy(replacement.toCharArray(), 0, digits, begin, replacement.length());
        }
        digits[lastDigitOnReplacement] = '0';
        int cs = luhnSum(digits);
        int parity = (digits.length - end) & 1;
        digits[lastDigitOnReplacement] = digitForLuhnComplement[parity][cs % 10];

        return new String(digits);
    }

    private static int luhnSum(char[] digits) {
        int parity = 0;
        int sum = 0;
        for (int n = digits.length; --n >= 0; ) {
            final char digit = digits[n];
            if (!Character.isDigit(digit)) {
                throw new IllegalArgumentException(String.format("non-decimal character at %d on '%s'", n, new String(digits)));
            }
            sum += luhnWeight[parity][digit - '0'];
            parity ^= 1;
        }
        return sum;
    }
}
