package br.com.muxi.ebo.cards

import spock.lang.Specification
import spock.lang.Unroll

class LuhnSpec extends Specification {
    @Unroll
    def "Validate checksum for credit card number #cardNumber"() {
        expect: "a valid card number"
            Luhn.isValid(cardNumber)

        where:
            cardNumber << [
                    "5384535003277290",
                    "5521280903613864",
                    "5447317400171748",
                    "5021213231744791",
                    "4191903004092868",
                    "110033225544776688990",
                    "11003322556"
        ]
    }

    @Unroll
    def "Must replace credit card number #cardNumber with #replacement and results in #expectedResult"() {
        when: 'the card number is partially replaced at offset=#begin by a number of #replacement.length() bytes long'
            def result = Luhn.replace(cardNumber, begin, replacement)

        then: "the result must match the expected value"
            result == expectedResult

        where:
            cardNumber              | replacement           | begin || expectedResult
            "____________________0" | "1100332255447766889" | 0     || "110033225544776688990"
            "1__________________90" |  "10033225544776688"  | 1     || "110033225544776688990"
            "11________________990" |   "003322554477668"   | 2     || "110033225544776688990"
            "110______________8990" |    "0332255447766"    | 3     || "110033225544776688990"
            "1100____________88990" |     "33225544776"     | 4     || "110033225544776688990"
            "11003__________688990" |      "322554477"      | 5     || "110033225544776688990"
            "110033________6688990" |       "2255447"       | 6     || "110033225544776688990"
            "1100332______76688990" |        "25544"        | 7     || "110033225544776688990"
            "11003322____776688990" |         "554"         | 8     || "110033225544776688990"
            "110033225__4776688990" |          "5"          | 9     || "110033225544776688990"
            "1100332___44776688990" |        "25"           | 7     || "110033225544776688990"
            "110033___544776688990" |       "22"            | 6     || "110033225544776688990"
            "11003___5544776688990" |      "32"             | 5     || "110033225544776688990"
            "1100___25544776688990" |     "33"              | 4     || "110033225544776688990"
            "110___225544776688990" |    "03"               | 3     || "110033225544776688990"
            "11___3225544776688990" |   "00"                | 2     || "110033225544776688990"
            "1___33225544776688990" |  "10"                 | 1     || "110033225544776688990"
            "___033225544776688990" | "11"                  | 0     || "110033225544776688990"
            "__0033225544776688990" | "1"                   | 0     || "110033225544776688990"
    }
}